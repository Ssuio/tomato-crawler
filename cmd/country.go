package cmd

import (
	"errors"
	"fmt"
	"strings"

	"github.com/gocolly/colly"
	"github.com/urfave/cli"
)

func (crawl *Crawler) topSitesCountry(countryCode string, countryTable map[string]string) ([]string, error) {
	if _, ok := countryTable[countryCode]; !ok {
		return nil, errors.New("Country code not existed")
	}
	return crawl.topSites(20, "https://www.alexa.com/topsites/countries/"+countryCode), nil
}

func (crawl *Crawler) findCounties(url string) map[string]string {
	countryTable := map[string]string{}

	// c.AllowedDomains = []string{"www.alexa.com"}
	crawl.c.OnHTML("ul.countries.span3", func(e *colly.HTMLElement) {
		e.ForEach("li", func(_ int, el *colly.HTMLElement) {
			countryCode := el.ChildAttr("a", "href")
			countryCode = strings.Split(countryCode, "/")[1]
			fullName := el.ChildText("a")
			countryTable[countryCode] = fullName
		})
	})

	crawl.c.Visit(url)
	crawl.c.Wait()
	return countryTable
}

func init() {
	CmdMgr.registerCmd(
		cli.Command{
			Name:  "country",
			Usage: "Show top 20 sites URL on www.alexa.com by country.",
			Action: func(c *cli.Context) error {
				crawler := NewCrawler()
				countryTable := crawler.findCounties("https://www.alexa.com/topsites/countries")
				sites, err := crawler.topSitesCountry(c.Args().First(), countryTable)
				if err != nil {
					return err
				}
				for _, url := range sites {
					fmt.Println(url)
				}
				return nil
			},
		},
	)
}
