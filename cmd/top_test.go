package cmd

import (
	"fmt"
	"net/http"
	"path/filepath"
	"testing"

	"github.com/gocolly/colly"
)

func TestTopSites(t *testing.T) {

	tsp := &http.Transport{}
	tsp.RegisterProtocol("file", http.NewFileTransport(http.Dir("/")))
	c := colly.NewCollector()
	c.WithTransport(tsp)

	abs, err := filepath.Abs("../test/topsites.html")
	if err != nil {
		panic(err)
	}

	crawler := Crawler{
		c,
	}

	sites := crawler.topSites(10, "file://"+abs)

	if len(sites) != 10 {
		t.Error("Sites number is wrong")
	}

	if sites[0] != "Google.com" {
		t.Error("Topsites order wrong")
	}

	fmt.Println(sites)
}
