package cmd

import (
	"net/http"
	"path/filepath"
	"testing"

	"github.com/gocolly/colly"
)

func TestCountries(t *testing.T) {

	tsp := &http.Transport{}
	tsp.RegisterProtocol("file", http.NewFileTransport(http.Dir("/")))
	c := colly.NewCollector()
	c.WithTransport(tsp)

	abs, err := filepath.Abs("../test/countries.html")
	if err != nil {
		panic(err)
	}

	crawler := Crawler{
		c,
	}

	countryTable := crawler.findCounties("file://" + abs)
	if _, ok := countryTable["TW"]; !ok {
		t.Error("TW not found in countryTable.")
	}

	sites, err := crawler.topSitesCountry("TW", countryTable)
	if err != nil {
		t.Error("TW sites parse error.", err)
	}

	if len(sites) != 20 {
		t.Error("TW sites count wrong.")
	}
}
