package cmd

import (
	"fmt"
	"strconv"

	"github.com/gocolly/colly"
	"github.com/urfave/cli"
)

func (crawl *Crawler) topSites(num int, url string) []string {
	tops := []string{}

	crawl.c.OnHTML("div.listings.table", func(e *colly.HTMLElement) {
		e.ForEach("div.tr.site-listing", func(_ int, el *colly.HTMLElement) {
			tops = append(tops, el.ChildText("div.td.DescriptionCell"))
		})
	})

	crawl.c.Visit(url)
	return tops[:num]
}

func (crawl *Crawler) topSitesGlobal(num int) []string {
	return crawl.topSites(num, "https://www.alexa.com/topsites")
}

func init() {
	CmdMgr.registerCmd(
		cli.Command{
			Name:  "top",
			Usage: "Show top <number> sites URL on www.alexa.com, number must be positive.",
			Action: func(c *cli.Context) error {
				num, err := strconv.Atoi(c.Args().First())
				if err != nil {
					return err
				}
				crawler := NewCrawler()
				sites := crawler.topSitesGlobal(num)
				for _, url := range sites {
					fmt.Println(url)
				}
				return nil
			},
		},
	)
}
