package cmd

import (
	"github.com/gocolly/colly"
	"github.com/urfave/cli"
)

var CmdMgr *CmdManager = &CmdManager{}

type CmdManager struct {
	Cmds []cli.Command
}

func (mgr *CmdManager) GetCmds() []cli.Command {
	return mgr.Cmds
}

func (mgr *CmdManager) registerCmd(cmd cli.Command) {
	mgr.Cmds = append(mgr.Cmds, cmd)
}

type Crawler struct {
	c *colly.Collector
}

func NewCrawler() *Crawler {
	c := colly.NewCollector()
	c.AllowedDomains = []string{"www.alexa.com"}
	return &Crawler{
		c,
	}
}
