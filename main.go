package main

import (
	"log"
	"os"

	"tomato-crawler/cmd"

	"github.com/urfave/cli"
)

func main() {
	app := &cli.App{
		Commands: cmd.CmdMgr.GetCmds(),
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
