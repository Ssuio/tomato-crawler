# Crawler for Alexa Topsites

This is a crawler to serve topsites url in global or in country.

## Usage

### TopSites with Number

Limit number of top sites.

`./bin/crawl top 10`

### TopSites with Number

Using country code.

`./bin/crawl country TW`

## Test

`go test -v ./...`

## Development

### Add SubCommand

1. Create a go file in ./cmd .
2. Create command action by `cli.Command`.
3. Register the command to command manager in init func.

```go
    CmdMgr.registerCmd(
		cli.Command{
			Name:  "country",
			Usage: "Show top 20 sites URL on www.alexa.com by country.",
			Action: func(c *cli.Context) error {
				crawler := NewCrawler()
				countryTable := crawler.findCounties("https://www.alexa.com/topsites/countries")
				sites, err := crawler.topSitesCountry(c.Args().First(), countryTable)
				if err != nil {
					return err
				}
				for _, url := range sites {
					fmt.Println(url)
				}
				return nil
			},
		},
	)
```